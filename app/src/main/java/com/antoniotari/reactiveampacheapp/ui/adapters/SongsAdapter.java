/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import com.antoniotari.audiosister.AudioSister;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.ui.views.PowerToast;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import static com.antoniotari.reactiveampacheapp.utils.Utils.setHtmlString;

/**
 * Created by antonio tari on 2016-05-22.
 */
public class SongsAdapter extends RecyclerView.Adapter<SongViewHolder> {

    public interface OnAddToPlaylistClickListener {
        void onAddClicked(Song song, int position);
    }

    public interface OnRemoveToPlaylistClickListener {
        void onRemoveClicked(Song song, int position);
    }

    public interface OnSongMenuClickListener {
        void onSongMenuClicked(Song song, final View anchor);
    }

    protected List<Song> mSongList;
    private boolean showArtist = false;
    private OnAddToPlaylistClickListener mOnAddToPlaylistClickListener;
    private OnRemoveToPlaylistClickListener mOnRemoveToPlaylistClickListener;
    private OnSongMenuClickListener mSongMenuClickListener;

    public SongsAdapter(List<Song> songs, final OnAddToPlaylistClickListener onAddToPlaylistClickListener,
            OnSongMenuClickListener songMenuClickListener) {
        mSongList = songs;
        mOnAddToPlaylistClickListener = onAddToPlaylistClickListener;
        mSongMenuClickListener = songMenuClickListener;
    }

    public SongsAdapter(List<Song> songs, final OnRemoveToPlaylistClickListener onRemoveToPlaylistClickListener,
            OnSongMenuClickListener songMenuClickListener) {
        mSongList = songs;
        mOnRemoveToPlaylistClickListener = onRemoveToPlaylistClickListener;
        mSongMenuClickListener = songMenuClickListener;
    }

    public SongsAdapter(List<Song> songs) {
        mSongList = songs;
    }

    public void setSongs(List<Song> items) {
        mSongList = items;
    }

    public void setShowArtist(final boolean showArtist) {
        this.showArtist = showArtist;
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_song, viewGroup, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder viewHolder, int i) {
        final Song song = mSongList.get(i);
        String title = (showArtist ? (song.getArtist().getName() + " - ") : "") + song.getTitle();
        setHtmlString(viewHolder.songName, title);

        int durationSecs = song.getTime();
        int minutes = song.getTime() / 60;
        int seconds = durationSecs - (minutes * 60);
        String secondsStr = seconds < 10 ? ("0" + seconds) : String.valueOf(seconds);
        viewHolder.songDuration.setText(minutes + ":" + secondsStr);

        //viewHolder.songNumber.setText(String.valueOf(song.getTrack()));
        Utils.loadImage(song.getArt(), viewHolder.songImage);

        viewHolder.addToPlaylistBtn.setVisibility(mOnAddToPlaylistClickListener != null ? View.VISIBLE : View.GONE);
        viewHolder.removePlaylist.setVisibility(mOnRemoveToPlaylistClickListener != null ? View.VISIBLE : View.GONE);
        viewHolder.songMenuButton.setVisibility(mSongMenuClickListener != null ? View.VISIBLE : View.GONE);

        viewHolder.addToPlaylistBtn.setOnClickListener(btn -> {
            if (mOnAddToPlaylistClickListener != null) {
                mOnAddToPlaylistClickListener.onAddClicked(song, i);
            }
        });

        viewHolder.removePlaylist.setOnClickListener(v ->
                Toast.makeText(viewHolder.removePlaylist.getContext(),R.string.song_long_press_remove, Toast.LENGTH_LONG).show());

        viewHolder.removePlaylist.setOnLongClickListener(v -> {
            if(mOnRemoveToPlaylistClickListener!=null) {
                mOnRemoveToPlaylistClickListener.onRemoveClicked(song,i);
            }
            return true;
        });

        Toast waitToast = new PowerToast(viewHolder.mainCardView.getContext(),R.string.wait_song);//Toast.makeText(viewHolder.mainCardView.getContext(),R.string.wait_song, Toast.LENGTH_LONG);
        viewHolder.mainCardView.setOnClickListener(view -> onSongClick(song, waitToast));

        viewHolder.songMenuButton.setOnClickListener( v -> {
            if(mSongMenuClickListener!=null) {
                mSongMenuClickListener.onSongMenuClicked(song,v);
            }
        });
    }

    private void onSongClick(final Song song, final Toast waitToast) {
        waitToast.show();
        // some times the play action freezes the ui for a few milliseconds
        // post delay the play action to allow the ripple effect to take place
        // it also gives some time to the toast to show
        new Handler(Looper.getMainLooper()).postDelayed( () -> {
            AudioSister.getInstance().playNew(song.getUrl(), song.getArtist().getName() + " - " + song.getTitle());
            PlayManager.INSTANCE.setSongs(mSongList);
            PlayManager.INSTANCE.setCurrentSong(song);
        }, 400);
    }

    @Override
    public int getItemCount() {
        if (mSongList == null) {
            return 0;
        }
        return mSongList.size();
    }
}