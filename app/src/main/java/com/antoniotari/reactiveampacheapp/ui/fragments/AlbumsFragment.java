/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampacheapp.ui.adapters.AlbumsAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.AlbumsAdapter.OnAlbumClickListener;

/**
 * Created by antonio tari on 2016-05-23.
 */
public class AlbumsFragment extends BaseFragment {

    private static final String KEY_STATE_ALBUMS = "com.antoniotari.reactiveampacheapp.ui.fragments.state.albums";

    private AlbumsAdapter mAlbumsAdapter;
    private ArrayList<Album> mAlbums;
    private OnAlbumClickListener mOnAlbumClickListener;

    public AlbumsFragment() {
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnAlbumClickListener){
            mOnAlbumClickListener = (OnAlbumClickListener) activity;
        } else {
            throw new RuntimeException("activity MUST implement OnAlbumClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnAlbumClickListener = null;
    }

    @Override
    protected void onRefresh() {
        AmpacheApi.INSTANCE.handshake()
                .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getAlbums())
                .subscribe(albumList -> {
                    initAdapter(albumList);
                    stopWaiting();
                }, this::onError);
    }

    @Override
    protected void initialize() {
        AmpacheApi.INSTANCE.getAlbums()
                .subscribe(this::initAdapter, this::onError);
    }

    private void initAdapter(List<Album> albumList) {
        stopWaiting();
        if (albumList == null) return;

        mAlbums = new ArrayList<>(albumList);
        if (mAlbumsAdapter == null) {
            mAlbumsAdapter = new AlbumsAdapter(albumList);
            mAlbumsAdapter.setOnAlbumClickListener(mOnAlbumClickListener);
            recyclerView.setAdapter(mAlbumsAdapter);
        } else {
            mAlbumsAdapter.setAlbums(albumList);
            mAlbumsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState!=null && savedInstanceState.getParcelableArrayList(KEY_STATE_ALBUMS)!=null){
            mAlbums = savedInstanceState.getParcelableArrayList(KEY_STATE_ALBUMS);
            initAdapter(mAlbums);
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_STATE_ALBUMS, mAlbums);
    }
}