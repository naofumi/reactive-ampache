/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.util.List;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampacheapp.ui.adapters.SongsAdapter;

import rx.android.observables.AndroidObservable;

/**
 * Created by antonio tari on 2016-05-23.
 */
public class SongsFragment extends BaseFragment {

    private SongsAdapter mSongsAdapter;

    public SongsFragment() {
    }

    @Override
    protected void onRefresh() {
        AndroidObservable.bindFragment(this,
                AmpacheApi.INSTANCE.handshake()
                        .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getSongs()))
                .subscribe(songList -> {
                    initAdapter(songList);
                    stopWaiting();
                }, this::onError);
    }

    @Override
    protected void initialize() {
        AndroidObservable.bindFragment(this,
                AmpacheApi.INSTANCE.getSongs())
                .subscribe(this::initAdapter, this::onError);
    }

    @Override
    protected void initViews(final View rootView) {
        super.initViews(rootView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initAdapter(List<Song> songList) {
        stopWaiting();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (mSongsAdapter == null) {
            mSongsAdapter = new SongsAdapter(songList);
            mSongsAdapter.setShowArtist(true);
            recyclerView.setAdapter(mSongsAdapter);
        } else {
            mSongsAdapter.setSongs(songList);
            mSongsAdapter.notifyDataSetChanged();
        }
    }
}