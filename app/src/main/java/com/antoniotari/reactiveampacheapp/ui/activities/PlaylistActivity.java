/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;

import java.util.List;
import java.util.Random;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;
import com.antoniotari.reactiveampacheapp.ui.adapters.LocalPlaylistAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.LocalPlaylistAdapter.OnSongMovedListener;
import com.antoniotari.reactiveampacheapp.ui.adapters.SongsAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.dragdrop.SimpleItemTouchHelperCallback;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import rx.android.observables.AndroidObservable;

/**
 * Created by antonio tari on 2016-06-21.
 */
public class PlaylistActivity extends AlbumActivity {

    public static final String KEY_INTENT_PLAYLIST = "com.antoniotari.reactiveampacheapp.playlistactivity.id";
    private Playlist playlist;

    protected Playlist getPlaylistData() {
        return playlist = getIntent().getParcelableExtra(KEY_INTENT_PLAYLIST);
    }

    @Override
    protected void initOnCreate() {
        playlist = getPlaylistData();

        setDetailName(playlist.getName());

        if (isLocalPlaylist()) {
            initSongList(((LocalPlaylist)playlist).getSongList());
        } else {
            AndroidObservable.bindActivity(this,
                    AmpacheApi.INSTANCE.getPlaylistSongs(playlist.getId()))
                    .subscribe(this::initSongList, this::onError);
        }

//        if (mLastFmArtist != null) {
//            loadArtistImage(mLastFmArtist, mBackgroundImage, true);
//        } else {
//            // get artist info from last.fm
//            artistInfoService(mAlbum.getArtist().getName())
//                    .subscribe(lastFmArtist -> {
//                        mLastFmArtist = lastFmArtist;
//                        loadArtistImage(lastFmArtist, mBackgroundImage, true);
//                    }, this::onError);
//        }
    }

    private void initSongList(List<Song> songList) {
        if (songList.size() == 0) return;

        initAdapter(songList);
        int randomSong = songList.size() == 1 ? 0 : new Random().nextInt(songList.size() - 1);

//                    String imageUrl = Utils.httpsToHttp(songList.get(randomSong).getArt());
//                    Picasso.with(this).load(imageUrl).into(mToolbarImageView);
        Utils.loadImage(songList.get(randomSong).getArt(), mToolbarImageView);
        Log.blu(songList.get(randomSong).getArt());

        // get artist info from last.fm
        randomSong = songList.size() == 1 ? 0 : new Random().nextInt(songList.size() - 1);

        artistInfoService(songList.get(randomSong).getArtist().getName())
                .subscribe(lastFmArtist -> {
                    mLastFmArtist = lastFmArtist;
                    loadArtistImage(lastFmArtist, mBackgroundImage, true);
                }, this::onError);
    }

    @Override
    protected void onRefresh() {
        if (isLocalPlaylist() || playlist==null) {
            swipeLayout.setRefreshing(false);
            return;
        }

        AndroidObservable.bindActivity(this,
                AmpacheApi.INSTANCE.handshake()
                        .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getPlaylistSongs(playlist.getId())))
                .subscribe(songList -> {
                    initAdapter(songList);
                    swipeLayout.setRefreshing(false);
                }, this::onError);
    }

    @Override
    protected void initAdapter(List<Song> songList) {
        if(isLocalPlaylist()) {
            initAdapterForLocalPlaylist(songList);
        } else {
            initAdapterForRemotePlaylist(songList);
        }
    }

    ItemTouchHelper mItemTouchHelper;

    protected void initAdapterForRemotePlaylist(List<Song> songList) {
        super.initAdapter(songList);
    }

    protected void initAdapterForLocalPlaylist(List<Song> songList) {
        stopWaiting();

        mSongsAdapter = new LocalPlaylistAdapter(songList, null, PlaylistActivity.this::onSongMenuClicked, this::onStartDragSong);

        // for drag and drop
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback((LocalPlaylistAdapter) mSongsAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);

        ((LocalPlaylistAdapter) mSongsAdapter).setOnSongMovedListener(new OnSongMovedListener() {
            @Override
            public void onSongMoved(final int fromPosition, final int toPosition, final List<Song> newList) {

                // if the ending position is -1 the song has been removed
                if (toPosition == -1){
                    onSongRemoved(fromPosition, newList);
                } else {
                    // update the play manager with the new playlist
                    PlayManager.INSTANCE.replacePlaylist(newList);
                    // update the playlist manager, only if the playlist is not the current
                    if (!isCurrentPlaylist(playlist)) {
                        PlaylistManager.INSTANCE.addPlaylist(playlist.getName(), newList);
                    }
                }
            }
        });

        showArtistNameInAdapter(mSongsAdapter);
        recyclerView.setAdapter(mSongsAdapter);
    }

    /**
     * when a song in given position is removed from the list
     * @param position
     */
    protected void onSongRemoved(int position, List<Song> newList) {
        // check if this is the current playing playlist
        if (isCurrentPlaylist(playlist)) {
            PlayManager.INSTANCE.removeSong(position);
        }

        PlaylistManager.INSTANCE.removeFromPlaylist(playlist.getName(), position);
        ((LocalPlaylist) playlist).getSongList().remove(position);

        if (((LocalPlaylist) playlist).getSongList().isEmpty()) {
            finish();
        } else {
            mSongsAdapter.notifyItemRemoved(position);
            //initAdapter(((LocalPlaylist) playlist).getSongList());
        }
    }

    private boolean isCurrentPlaylist(Playlist playlist1) {
        //FIXME: this is not a good way to recognize the current playlist
        return (playlist1.getName().equalsIgnoreCase(getString(R.string.current_playlist)));
    }

    private void onStartDragSong(final ViewHolder viewHolder) {
        if (mItemTouchHelper != null) {
            mItemTouchHelper.startDrag(viewHolder);
        }
    }

    @Override
    protected void showArtistNameInAdapter(SongsAdapter songsAdapter) {
        songsAdapter.setShowArtist(true);
    }

    protected boolean isMenuRemoveVisible() {
        return isLocalPlaylist();
    }

    @Override
    protected boolean onMenuItemClick(final MenuItem item, final Song song) {
        if (item.getItemId() == R.id.menu_remove) {
            // this is always true, because otherwise isMenuRemoveVisible() would hide this item
            if (isLocalPlaylist()) {
                List<Song> songList = ((LocalPlaylist) playlist).getSongList();
                onSongRemoved(songList.indexOf(song), null);
            }
            return true;
        }
        return super.onMenuItemClick(item, song);
    }

    private boolean isLocalPlaylist() {
        return playlist instanceof LocalPlaylist;
    }
}
