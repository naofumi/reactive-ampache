/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.antoniotari.android.lastfm.LastFmArtist;
import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampacheapp.R;

import static com.antoniotari.reactiveampacheapp.utils.Utils.setHtmlString;

/**
 * Created by antonio tari on 2016-05-21.
 */
public class AlbumsAdapter extends RecyclerView.Adapter<GridViewHolder> {

    public interface OnAlbumClickListener {
        void onAlbumClick(final Album album, final LastFmArtist lastFmArtist);
    }

    private List<Album> mItems;
    private OnAlbumClickListener mOnAlbumClickListener;
    private LastFmArtist mLastFmArtist;

    public AlbumsAdapter(List<Album> items) {
        mItems = items;
    }

    public void setOnAlbumClickListener(final OnAlbumClickListener onAlbumClickListener) {
        mOnAlbumClickListener = onAlbumClickListener;
    }

    public void setLastFmArtist(LastFmArtist lastFmArtist) {
        mLastFmArtist = lastFmArtist;
    }

    public void setAlbums(List<Album> albumList) {
        mItems = albumList;
    }

    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_artist, viewGroup, false);

        return new GridViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GridViewHolder viewHolder, int i) {
        Context context = viewHolder.imageView.getContext();
        final Album album = mItems.get(i);
        setHtmlString(viewHolder.primaryTextView, album.getName());
        setHtmlString(viewHolder.secondaryLeftTextView, album.getArtist().getName());
        String songStr = album.getTracks() == 1 ? context.getString(R.string.song) : context.getString(R.string.songs);
        viewHolder.secondaryRightTextView.setText(String.format(songStr, album.getTracks()));

        // if the art is not available use last to fetch it
        if (album.getArt()!=null){
            viewHolder.loadImage(album.getArt());
        } else {
            viewHolder.loadLastFmImage(album.getArtist().getName(), album.getName());
        }

        viewHolder.mainCardView.setOnClickListener(view -> {
            if (mOnAlbumClickListener != null) {
                mOnAlbumClickListener.onAlbumClick(album, mLastFmArtist);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mItems == null) {
            return 0;
        }
        return mItems.size();
    }
}