/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp;

import android.app.Application;

import com.antoniotari.android.lastfm.LastFm;
import com.antoniotari.audiosister.AudioSister;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by antonio tari on 2016-05-24.
 */
public class AmpApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        PlaylistManager.INSTANCE.init(this);
        // init last fm and ampache library
        LastFm.INSTANCE.init(BuildConfig.LASTFM_API_KEY);
        AmpacheApi.INSTANCE.initSession(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AudioSister.getInstance().kill();
    }
}
