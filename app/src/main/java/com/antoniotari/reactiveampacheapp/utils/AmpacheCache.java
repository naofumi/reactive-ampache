package com.antoniotari.reactiveampacheapp.utils;

import android.support.v4.util.ArrayMap;

import java.util.Map;

import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Artist;
import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;

/**
 * Created by antoniotari on 2017-01-09.
 */

public enum AmpacheCache {
    INSTANCE;

    private final Map<String,Artist> mArtistMap = new ArrayMap<>();
    private final Map<String,Album> mAlbumMap = new ArrayMap<>();
    private final Map<String,Song> mSongMap = new ArrayMap<>();
    private final Map<String,Playlist> mPlaylistMap = new ArrayMap<>();

    public void addArtistToCache(Artist artist) {
        mArtistMap.put(artist.getId(),artist);
    }

    public void addAlbumToCache(Album album) {
        mAlbumMap.put(album.getId(),album);
    }

    public void addPlaylistToCache(Playlist playlist) {
        mPlaylistMap.put(playlist.getName(), playlist);
    }

    public void addSongToCache(Song song) {
        mSongMap.put(song.getId(),song);
    }

    public Artist getArtist(String id) {
        return mArtistMap.get(id);
    }

    public Album getAlbum(String id) {
        return mAlbumMap.get(id);
    }

    public Song getSong(String id) {
        return mSongMap.get(id);
    }

    public Playlist getPlaylist(final String playlistName) {
        return mPlaylistMap.get(playlistName);
    }
}
