/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.utils;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antoniotari.reactiveampache.Exceptions.AmpacheApiException;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.R;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

/**
 * Created by antonio tari on 2016-05-29.
 */
public class Utils {

    public static void onError(Context context, Throwable throwable) {
        Crashlytics.logException(throwable);
        Log.error(throwable);
        String message;
        if (throwable instanceof AmpacheApiException) {
            AmpacheApiException ampacheApiException = (AmpacheApiException) throwable;
            message = "Ampache error\ncode:" + ampacheApiException.getAmpacheError().getCode() + "\nerror: " +
                    ((AmpacheApiException) throwable).getAmpacheError().getError();

            // 999 is undefined error
            if (ampacheApiException.getAmpacheError().getCode() != null
                    && ampacheApiException.getAmpacheError().getError() != null
                    && !ampacheApiException.getAmpacheError().getCode().equalsIgnoreCase("999")
                    && !ampacheApiException.getAmpacheError().getError().equalsIgnoreCase("TODO")) {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        } else if (throwable.getLocalizedMessage() != null) {
            message = throwable.getLocalizedMessage();
        } else {
            message = "Undefined error";
        }
        Log.error(message);
    }

    /**
     * temporary fix to picasso issue with https
     */
    public static String httpsToHttp(String url) {
        if (!TextUtils.isEmpty(url) && url.length() > 5 && url.substring(0, 5).equals("https")) {
            return url.replace("https", "http");
        }
        return url;
    }

    public static void setHtmlString(TextView textView, String string) {
        if (string == null) string = "";
        Spanned htmlString = null;
        try {
            htmlString = Html.fromHtml(string);
            if (TextUtils.isEmpty(htmlString.toString())) {
                htmlString = null;
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        textView.setText(htmlString != null ? htmlString : string);
    }

    public static void loadImage(String url, ImageView imageView) {
        if(imageView == null)return;
        if(imageView.getContext() == null)return;

        if(TextUtils.isEmpty(url)) {
            imageView.setImageResource(R.drawable.album_art_placeholder);
            return;
        }

        String imageUrl = Utils.httpsToHttp(url);
        try {
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .error(R.drawable.album_art_placeholder)
                    .into(imageView);
        } catch (IllegalArgumentException e){
            Crashlytics.logException(e);
        }
    }
}
